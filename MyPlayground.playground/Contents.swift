import Foundation


class FibonachiSequence {
    func getNumber(at numberInSequence: UInt) -> Int {
        let a: Double = (1+sqrt(5))/2
        let b: Double = (1-sqrt(5))/2
        let result: Double = (pow(a, Double(numberInSequence)) - pow(b, Double(numberInSequence)))/sqrt(5)
        return Int(round(result))
    }
    func getFullSequence(until lastNumber: UInt) -> [Int]{
        var result = [Int]()
        for el in 0...lastNumber {
            result.append(getNumber(at: el))
        }
        return result
    }
}

let fibonachi = FibonachiSequence()
print("Fibonachi sequence:\n\(fibonachi.getFullSequence(until:12))")
print("Fibonachi number by id:\n\(fibonachi.getNumber(at: 91))") //max number of fibonachi we can provide with default Int is 92
print("--------------------------------------------------------------------------")


extension Array where Element: Comparable{
    func bubbleSorted(by compare: (Self.Element, Self.Element) -> Bool) -> [Element] {
        var sorted = self
        for i in (0...self.count - 2) {
            for j in (i...self.count - 1) {
                if compare(sorted[i], sorted[j]) {
                    sorted.swapAt(i, j)
                }
            }
        }
        return sorted
    }
    
    mutating func bubbleSort(by compare: (Self.Element, Self.Element) -> Bool) -> [Element] {
        for i in (0...self.count - 2) {
            for j in (i...self.count - 1) {
                if compare(self[i], self[j]) {
                    self.swapAt(i, j)
                }
            }
        }
        return self
    }
    
    func quickSorted() -> [Element] {
        guard self.count > 1 else { return self }
        let pivot = self[self.count/2]
        let less = self.filter { $0 < pivot }
        let equal = self.filter { $0 == pivot }
        let greater = self.filter { $0 > pivot}
        return less.quickSorted() + equal + greater.quickSorted()
    }
    
    mutating func quickSort() -> [Element] {
        guard self.count > 1 else { return self }
        let pivot = self[self.count/2]
        let less = self.filter { $0 < pivot }
        let equal = self.filter { $0 == pivot }
        let greater = self.filter { $0 > pivot}
        self = less.quickSorted() + equal + greater.quickSorted()
        return self
    }
    
    func binarySearch(predicate: (Element) -> Bool) -> Index {
            var low = startIndex
            var high = endIndex
            while low != high {
                let mid = index(low, offsetBy: distance(from: low, to: high)/2)
                if predicate(self[mid]) {
                    low = index(after: mid)
                } else {
                    high = mid
                }
            }
            return low
        }
}

var array = [1,4,5,345,435,34,32,44,25]
var bubbleTestArray = array
var bubbleSortedArray = bubbleTestArray.bubbleSorted(by: {(a:Int, b: Int) -> Bool in return a > b})
print("Initial array:\n\(bubbleTestArray)")
print("New bubble sorted array:\n\(bubbleSortedArray)")
print("Initial array after bubbleSorted method:\n\(bubbleTestArray)")
var bubbleSortArray = bubbleTestArray.bubbleSort(by: {(a:Int, b: Int) -> Bool in return a < b})
print("Initial array by refference after bubbleSort method:\n\(bubbleSortArray)")
print("Initial array after bubbleSort method:\n\(bubbleTestArray)")
print("--------------------------------------------------------------------------")

var quickTestArray = array
var quickSortedArray = quickTestArray.quickSorted()
print("Array from quickSorted method array:\n\(quickSortedArray)")
print("Initial array after quickSorted method:\n\(quickTestArray)")
var quickSortArray = quickTestArray.quickSort()
print("Initial array by refference after quickSort method:\n\(quickSortArray)")
print("Initial array after qucikSort method:\n\(quickTestArray)")
print("--------------------------------------------------------------------------")

var binarySearchItem = 2
var index = array.binarySearch(predicate: {$0 == binarySearchItem})
if index != 0 {
    print("Index of \(binarySearchItem) in \(array) is \(index)")
} else {
    print("Cant find element \(binarySearchItem) in \(array)")
}

